import mcScrollTo from 'mc-scrollto';

/**
 * mc-fixed JavaScript Library v1.0.0
 *
 * 此插件功能是通天塔层楼滚动到可见区域时发送埋点。
 *
 * @param  param          {*}           入参
 * @param  param.el       {Element}     需要监听的DOM节点
 * @param  param.wrap     {Element}     动态参考元素 (选填，如不填写则为其父元素)
 * @param  param.offset   {Number}      偏移量值 (选填，默认为0)
 * @param  param.toDown   {Boolean}     是否开启点击向下 (选填，默认为false)
 * @param  param.on       {Object}      回调集
 * @param  param.on.up    {Function}    回调 (选填)
 * @param  param.on.down  {Function}    回调 (选填)
 *
 *
 * 文档：https://gitlab.com/fekits/mc-arrow
 *
 * 版本：v1.0.0
 *
 * 作者：xiaojunbo
 *
 * 兼容：IE9+
 *
 */
let McArrow = function (param) {
  param = param || {};
  let doc = document;
  let name = 'mc-arrow';
  this.el = param.el || doc.createElement('div');
  this.wrap = param.wrap || doc.documentElement || doc.body;
  this.offset = param.offset || (doc.documentElement.offsetHeight || doc.body.offsetHeight) / 3;
  this.toDown = param.toDown;
  this.on = Object.assign({ up() {}, down() {} }, param.on || {});
  this.status = 'none';

  // 添加标识
  this.el.className += name;
  this.el.innerHTML = '<div class="' + name + '-icon"><i class="' + name + '-up"></i><i class="' + name + '-down"></i></div>';

  // 设置主题
  this.el.setAttribute('data-theme', param.theme || 'aa');

  // 获取滚动条距离
  let getScrollTop = () => { return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;};
  // 设置自定义数据
  let setData = (status) => {
    this.el.setAttribute('data-status', status);
    this.status = status;
  };

  // 隐藏图标
  setData('none');

  // 将图标插入到文档中
  this.wrap.appendChild(this.el);

  // 是否滚动到底部
  let isBottom = () => {
    let inHeight = window.innerHeight + 1;
    let rect = doc.documentElement.getBoundingClientRect() || doc.body.getBoundingClientRect();
    rect.isBottom = rect.bottom < inHeight;
    return rect.isBottom;
  };

  let McFixedRequestAnimFrame = (function () {return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {setTimeout(callback, 1000 / 60);};})();

  // 上一次滚动条距离
  let oldScrollTop = getScrollTop();

  // 当前是否正在处理中
  let working = 1;
  // 核心逻辑代码
  let mainCode = (isLoad) => {
    // 获取滚动距离
    let nowScrollTop = getScrollTop();
    // console.log(nowScrollTop, this.offset);
    // 滚动到配置位置时显示图标
    if (nowScrollTop > this.offset) {
      if (isLoad || isBottom() || oldScrollTop > nowScrollTop) {
        setData('up');
      } else if (oldScrollTop < nowScrollTop) {
        setData(this.toDown ? 'down' : 'up');
      }
    } else {
      setData('none');
    }
    oldScrollTop = nowScrollTop;
    working = 1;
  };

  mainCode(1);
  window.addEventListener('scroll', () => {
    McFixedRequestAnimFrame(() => {
      if (working) {
        working = 0;
        mainCode();
      }
    });
  });

  this.el.addEventListener('click', () => {
    // console.log(this.status);
    if (this.status === 'up') {

      if (param.on && param.on.up) {
        this.on.up();
      } else {
        mcScrollTo({
          to: 0
        });
      }
    }
    if (this.status === 'down') {
      // console.log(getScrollTop());
      // console.log(window.innerHeight);
      if (param.on && param.on.down) {
        this.on.down();
      } else {
        mcScrollTo({
          to: getScrollTop() + window.innerHeight
        });
      }
    }
  });

};

export default McArrow;
