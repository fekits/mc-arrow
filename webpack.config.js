const path = require('path');                                                                                            // NODE自带的用来获取文件路径的模块插件
const CleanWebpackPlugin = require('clean-webpack-plugin');                                                              // 清理文件或文件夹的模块插件
const HtmlWebpackPlugin = require('html-webpack-plugin');                                                                // 处理HTML的模块插件
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');                                                               // 压缩JS的模块插件
const MiniCssExtractPlugin = require('mini-css-extract-plugin');                                                         // 把CSS抽离成独立文件的模块插件
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');                                           // 压缩CSS的模块插件
const CopyWebpackPlugin = require('copy-webpack-plugin');

const dev = process.env.npm_lifecycle_event === 'serve';                                                                   // 是否为开发环境?

module.exports = {
  context: path.resolve(__dirname, './'),                                                                                // 项目基础路径(要用绝对路径)
  mode: dev ? 'development' : 'production',                                                                              // 设置开发环境
  devtool: dev ? 'eval-source-map' : 'none',                                                                             // 是否生成MAP源文件追踪？开如是开发环境则生成，生产环境则不生成
  entry: {                                                                                                               // 入口文件
    main: './src/js/main.js',
    demo1: './src/js/demo1.js',
    demo2: './src/js/demo2.js',
    demo3: './src/js/demo3.js'
  },
  output: {
    filename: dev ? '[name].js' : 'js/[name].min.js',                                                                    // 打包输出的文件名
    path: path.resolve(__dirname, './dist'),                                                                             // 打包输出路径(要用绝对路经)
    publicPath: dev ? '' : './',
    library: '[name]',                                                                                                   // 打包生成的模块名
    libraryTarget: 'umd',                                                                                                // 打包生成UMD格式
    libraryExport: 'default'                                                                                             // 设置生成UMD格式时，这个需要设置为"default"
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: dev
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['./dist/']),                                                                                 // 清理文件
    new MiniCssExtractPlugin({
      filename: dev ? '[name].css' : 'css/[name].[hash:5].min.css'
    }),
    new HtmlWebpackPlugin({                                                                                              // 处理HTML文件
      template: './src/index.html',
      filename: 'index.html',                                                                                            // 指定输出路径和文件名
      inject: true,
      showErrors: true,                                                                                                  // WEBPACK 编译出现错误
      minify: {                                                                                                          // 对 HTML 文件进行压缩，minify 的属性值是一个压缩选项或者 false 。默认值为false, 不对生成的 html 文件进行压缩
        removeComments: true,                                                                                            // 去除注释
        collapseWhitespace: true,                                                                                        // 是否去除空格
        minifyCSS: true,
        minifyJS: true
      },
      chunks: ['main']
    }),
    new HtmlWebpackPlugin({                                                                                              // 处理HTML文件
      template: './src/demo1.html',
      filename: 'demo1.html',                                                                                            // 指定输出路径和文件名
      inject: true,
      showErrors: true,                                                                                                  // WEBPACK 编译出现错误
      minify: {                                                                                                          // 对 HTML 文件进行压缩，minify 的属性值是一个压缩选项或者 false 。默认值为false, 不对生成的 html 文件进行压缩
        removeComments: true,                                                                                            // 去除注释
        collapseWhitespace: true,                                                                                        // 是否去除空格
        minifyCSS: true,
        minifyJS: true
      },
      chunks: ['main','demo1']
    }),
    new HtmlWebpackPlugin({                                                                                              // 处理HTML文件
      template: './src/demo2.html',
      filename: 'demo2.html',                                                                                            // 指定输出路径和文件名
      inject: true,
      showErrors: true,                                                                                                  // WEBPACK 编译出现错误
      minify: {                                                                                                          // 对 HTML 文件进行压缩，minify 的属性值是一个压缩选项或者 false 。默认值为false, 不对生成的 html 文件进行压缩
        removeComments: true,                                                                                            // 去除注释
        collapseWhitespace: true,                                                                                        // 是否去除空格
        minifyCSS: true,
        minifyJS: true
      },
      chunks: ['main','demo2']
    }),
    new HtmlWebpackPlugin({                                                                                              // 处理HTML文件
      template: './src/demo3.html',
      filename: 'demo3.html',                                                                                            // 指定输出路径和文件名
      inject: true,
      showErrors: true,                                                                                                  // WEBPACK 编译出现错误
      minify: {                                                                                                          // 对 HTML 文件进行压缩，minify 的属性值是一个压缩选项或者 false 。默认值为false, 不对生成的 html 文件进行压缩
        removeComments: true,                                                                                            // 去除注释
        collapseWhitespace: true,                                                                                        // 是否去除空格
        minifyCSS: true,
        minifyJS: true
      },
      chunks: ['main','demo3']
    }),
    new CopyWebpackPlugin([
      { from: './lib/theme', to: '../npm/theme' },
      { from: './README.md', to: '../npm/README.md' }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader'
        }
      },
      {
        test: /\.(scss|sass|css)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: dev ? 'style-loader' : MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'img/[name].[hash:7].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'media/[name].[hash:7].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[hash:7].[ext]'
            }
          }
        ]
      }
    ]
  },
  devServer: {
    hot: true,
    stats: 'errors-only',
    host: 'dev.m.jd.com'
  },
  stats: 'minimal'
};
