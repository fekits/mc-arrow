// 代码着色插件
import McTinting from 'mc-tinting';

new McTinting();

import McArrow from '../../lib/mc-arrow';

// 楼层埋点插件
new McArrow({
  toDown: true
});

