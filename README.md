# MC-FIXED
```$xslt
指定元素滚动到窗口指定位置时固定在指定位置
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[http://www.junbo.name/plugins/mc-arrow](http://www.junbo.name/plugins/mc-arrow)


#### 开始

下载项目: 

```npm
npm i mc-arrow
```

#### 参数
```$xslt
  @param  param          {Object}      入参
  @param  param.el       {Element}     需要监听的DOM节点
  @param  param.wrap     {Element}     动态参考元素 (选填，如不填写则为其父元素)
  @param  param.top      {String}      偏移量值 (选填，默认为0)
  @param  param.on       {Object}      回调集 (选填)
  @param  param.on.up    {Function}    向上时
  @param  param.on.down  {Function}    向下时
```

#### 示例

前置条件
```html
// 使用此插件，必须在需要固定的元素外面包一层容器，且此容器尺寸与位置必须和固定元素一样大小或不设置任何尺寸与位置属性。
<div>
  <div id="el"></div>
</div>
```

```javascript
// 楼层埋点插件
import McArrow from 'mc-arrow';

// 滚动到500PX时显示按钮。
new McArrow({
  offset: 500
});
```

#### 版本
```$xslt
v1.0.3
1. 非核心更新，新增文件到NPM
```

```$xslt
v1.0.2
1. 修改入参数默认值可以为空或不入参
```

```$xslt
v1.0.1
1. requestAnimationFrame添加兼容性前缀
```

```text
v1.0.0
1. 此版本完成了核心功能。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
